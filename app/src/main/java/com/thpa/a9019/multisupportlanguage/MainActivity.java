package com.thpa.a9019.multisupportlanguage;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.Locale;


public class MainActivity extends AppCompatActivity {

    private Button changlang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();
        setContentView(R.layout.activity_main);

        //Change action bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.app_name));

        changlang = (Button) findViewById(R.id.btn_chang);
        changlang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Show Alert Dialog to display list of language
                ShowChangeLanguageDialog();
            }
        });
    }

    private void ShowChangeLanguageDialog() {

        final String[] listItems = {"Spanish", "Japanese", "Thai", "Chinese", "English"};
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
        mBuilder.setTitle("Choose language.....");
        mBuilder.setSingleChoiceItems(listItems, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                 if (i == 0) {
                    //Span
                    setLocale("es");
                    recreate();
                } else if (i == 1) {
                    //Japan
                    setLocale("ja");
                    recreate();
                } else if (i == 2) {
                    //Thai
                    setLocale("th");
                    recreate();

                } else if (i == 3) {
                    //Chinese
                    setLocale("zh");
                    recreate();
                }

                else {
                     //English
                    setLocale("en");
                    recreate();
                }
                //dismiss alert dialog when language selected
                dialog.dismiss();
            }
        });
        AlertDialog mDialog = mBuilder.create();
        //show alert dialog
        mDialog.show();
    }

    private void setLocale(String lang) {

        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());
        //save data to shared preference
        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("My_Lang", lang);
        editor.apply();


    }

    //load language saved in shared preference
    public void loadLocale() {
        SharedPreferences preferences = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = preferences.getString("My_Lang", "");
        setLocale(language);
    }
}
